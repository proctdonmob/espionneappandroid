package lpirm.veille.espionneapp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import lpirm.veille.espionneapp.R;

/**
 * Created by Hp on 06/03/2018.
 */

public class MessageDialog extends Dialog {

    // widgets
    private TextView txvTitle, txvMessage;
    private Button btnAccept, btnCancel;

    public MessageDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_message);
        setCancelable(false);

        init();
    }

    private void init() {
        txvTitle = (TextView) findViewById(R.id.txv_title);
        txvMessage = (TextView) findViewById(R.id.txv_message);
        btnAccept = (Button) findViewById(R.id.btn_accept);
        setAcceptAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        setCancelAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    // getters
    public TextView getTxvTitle() {
        return txvTitle;
    }

    public TextView getTxvMessage() {
        return txvMessage;
    }

    // setters
    public void setTitle(String title) {
        txvTitle.setText(title);
    }

    public void setMessage(String message) {
        txvMessage.setText(message);
    }

    public void setAcceptAction(View.OnClickListener listener) {
        btnAccept.setOnClickListener(listener);
    }

    public void setCancelAction(View.OnClickListener listener) {
        btnCancel.setOnClickListener(listener);
    }
}
