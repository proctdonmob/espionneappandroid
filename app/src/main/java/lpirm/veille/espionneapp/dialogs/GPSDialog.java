package lpirm.veille.espionneapp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import lpirm.veille.espionneapp.R;

/**
 * Created by Hp on 15/02/2018.
 */

public class GPSDialog extends Dialog {

    // widgets
    private Button btnDaccord, btnDesaccord;

    public GPSDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_gps);
        setCancelable(false);

        init();
    }

    private void init() {
        btnDaccord = (Button) findViewById(R.id.btn_daccord);
        setDaccordAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnDesaccord = (Button) findViewById(R.id.btn_desaccord);
        setDesaccordAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    // setters
    public void setDaccordAction(View.OnClickListener listener) {
        btnDaccord.setOnClickListener(listener);
    }

    public void setDesaccordAction(View.OnClickListener listener) {
        btnDesaccord.setOnClickListener(listener);
    }
}
