package lpirm.veille.espionneapp.asynctasks;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.activities.LoginActivity;
import lpirm.veille.espionneapp.asynctasks.webservices.WebServices;
import lpirm.veille.espionneapp.models.User;
import lpirm.veille.espionneapp.systems.V;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hp on 06/03/2018.
 */

public class LoginAsyncTask extends AsyncTask<User, Void, User> {

    private LoginActivity loginActivity;
    private ImageView imvLauncher;
    private ProgressBar pgbLogin;
    private WebServices webServices;

    public LoginAsyncTask(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
        pgbLogin = loginActivity.findViewById(R.id.pgb_login);
        pgbLogin.setIndeterminate(true);
        imvLauncher = loginActivity.findViewById(R.id.imv_launcher);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebServices.SAMSUNG_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        webServices = retrofit.create(WebServices.class);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pgbLogin.setVisibility(View.VISIBLE);
        imvLauncher.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);

        loginActivity.onPostLoginAsyncTask(user);

        pgbLogin.setVisibility(View.INVISIBLE);
        imvLauncher.setVisibility(View.VISIBLE);
    }

    @Override
    protected User doInBackground(User... users) {
        User user = users[0];

        Call<User> call = webServices.login(user.username, user.password);
        try {
            Response<User> response = call.execute();
            if (response.isSuccessful())
                user = response.body();
            else
                user = null;
        } catch (IOException e) {
            e.printStackTrace();
            user = null;
        }

        return user;
    }
}
