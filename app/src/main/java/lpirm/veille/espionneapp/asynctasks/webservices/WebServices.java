package lpirm.veille.espionneapp.asynctasks.webservices;

import java.util.List;

import lpirm.veille.espionneapp.models.Device;
import lpirm.veille.espionneapp.models.File;
import lpirm.veille.espionneapp.models.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Hp on 06/03/2018.
 */

public interface WebServices {
    String HOME_HOST = "http://192.168.10.102:82/";
    String SAMSUNG_HOST = "http://192.168.43.249:82/";

    @FormUrlEncoded
    @POST("login")
    Call<User> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("signup")
    Call<User> signup(@Field("username") String username, @Field("password") String password,
                      @Field("image") String image);

    @FormUrlEncoded
    @POST("device")
    Call<Device> postDevice(@Field("name") String name, @Field("version") String version,
                                  @Field("lat") double lat, @Field("lng") double lng,
                                  @Field("user") int user);

    @FormUrlEncoded
    @POST("newfiles")
    Call<Device> postNewFiles(@Field("device_id") int device_id, @Field("files[]") List<String> files);

    @FormUrlEncoded
    @PATCH("device/{id}")
    Call<Device> patchLocation(@Path("id") int id, @Field("lat") double lat, @Field("lng") double lng);
}
