package lpirm.veille.espionneapp.asynctasks;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.activities.SignupActivity;
import lpirm.veille.espionneapp.asynctasks.webservices.WebServices;
import lpirm.veille.espionneapp.models.User;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hp on 06/03/2018.
 */

public class SignupAsyncTask extends AsyncTask<User, Void, User> {

    private SignupActivity signupActivity;
    private ImageView imvPicture;
    private ImageButton imbPicture;
    private ProgressBar pgbSignup;
    private WebServices webServices;

    public SignupAsyncTask(SignupActivity signupActivity) {
        this.signupActivity = signupActivity;
        pgbSignup = signupActivity.findViewById(R.id.pgb_signup);
        pgbSignup.setIndeterminate(true);
        imvPicture = signupActivity.findViewById(R.id.imv_picture);
        imbPicture = signupActivity.findViewById(R.id.imb_picture);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebServices.SAMSUNG_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        webServices = retrofit.create(WebServices.class);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pgbSignup.setVisibility(View.VISIBLE);
        imvPicture.setVisibility(View.INVISIBLE);
        imbPicture.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);

        signupActivity.onPostSignupAsyncTask(user);

        pgbSignup.setVisibility(View.INVISIBLE);
        imvPicture.setVisibility(View.VISIBLE);
        imbPicture.setVisibility(View.VISIBLE);
    }

    @Override
    protected User doInBackground(User... users) {
        User user = users[0];

        Call<User> call = webServices.signup(user.username, user.password, user.image);
        try {
            Response<User> response = call.execute();
            if (response.isSuccessful())
                user = response.body();
            else
                user = null;
        } catch (IOException e) {
            e.printStackTrace();
            user = null;
        }

        return user;
    }
}
