package lpirm.veille.espionneapp.asynctasks;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.asynctasks.webservices.WebServices;
import lpirm.veille.espionneapp.models.Device;
import lpirm.veille.espionneapp.services.EspionService;
import lpirm.veille.espionneapp.systems.V;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hp on 07/03/2018.
 */

public class PatchLocationAsyncTask extends AsyncTask<Device, Void, Device> {

    private EspionService espionService;
    private WebServices webServices;

    public PatchLocationAsyncTask(EspionService espionService) {
        this.espionService = espionService;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebServices.SAMSUNG_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        webServices = retrofit.create(WebServices.class);
    }

    @Override
    protected void onPostExecute(Device device) {
        super.onPostExecute(device);

        espionService.onPostPathLocation(device);
    }

    @Override
    protected Device doInBackground(Device... devices) {
        Device device = devices[0];

        Call<Device> call = webServices.patchLocation(device.id, device.lat, device.lng);
        try {
            Response<Device> response = call.execute();
            if (response.isSuccessful())
                device = response.body();
            else
                device = null;
        } catch (IOException e) {
            e.printStackTrace();
            device = null;
        }

        return device;
    }
}
