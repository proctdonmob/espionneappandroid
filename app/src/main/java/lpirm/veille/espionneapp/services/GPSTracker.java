package lpirm.veille.espionneapp.services;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;

import lpirm.veille.espionneapp.dialogs.GPSDialog;
import lpirm.veille.espionneapp.dialogs.MessageDialog;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by Hp on 07/03/2018.
 */

public class GPSTracker extends Service implements LocationListener {
    // tag variable
    public static final int REQUEST_LOCATION = 1;

    // contexts values
    private final Context context;
    private final Activity activity;

    // states values
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;

    // location variables
    private Location location;
    protected LocationManager locationManager;

    // settings variables
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // metros
    private static final long MIN_TIME_BW_UPDATES = 1000; // milisegundos

    // interfaces objects
    private LocationChanged locationChanged;

    // constructors
    public GPSTracker(Context context) {
        this.context = context;
        this.activity = null;
        getLocation();
    }

    public GPSTracker(Activity activity) {
        this.context = this.activity = activity;
        getLocation();
    }

    // setters
    public void setOnLocationChanged(LocationChanged locationChanged) {
        this.locationChanged = locationChanged;
    }

    // methods
    public Location getLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            try {
                locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
                // GPS acik mi?
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // Internet acik mi?
                isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (!isGPSEnabled && !isNetworkEnabled) {
                    showSettingsAlert();
                } else {
                    this.canGetLocation = true;

                    // Once internetten alinan konum bilgisi kayitlanir
                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("Network", "Network");
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        }
                    }
                    // GPS'ten alinan konum bilgisi;
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            Log.d("GPS Enabled", "GPS Enabled");
                            if (locationManager != null) {
                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (locationChanged != null)
            locationChanged.onLocationChanged(location);
        return location;
    }

    public double getLatitude() {
        if (location != null) {
            return location.getLatitude();
        }

        return 0;
    }

    // Boylam bilgisini dondurur
    public double getLongitude() {
        if (location != null) {
            return location.getLongitude();
        }

        return 0;
    }

    public LatLng getLatLng() {
        if (location != null) {
            return new LatLng(location.getLatitude(), location.getLongitude());
        }
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (locationChanged != null)
            locationChanged.onLocationChanged(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public void showSettingsAlert() {
        final GPSDialog dialog = new GPSDialog(context);
        dialog.setDaccordAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    // LocationManager'in gps isteklerini durdurur
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    // interfaces
    public interface LocationChanged {
        void onLocationChanged(Location location);
    }

    private void showMessageDialog(String title, String message, View.OnClickListener acceptAction) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setAcceptAction(acceptAction);
        dialog.show();
    }
}
