package lpirm.veille.espionneapp.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import lpirm.veille.espionneapp.dialogs.MessageDialog;
import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.asynctasks.LoginAsyncTask;
import lpirm.veille.espionneapp.models.User;
import lpirm.veille.espionneapp.services.EspionReceiver;
import lpirm.veille.espionneapp.services.EspionService;
import lpirm.veille.espionneapp.systems.EspionPreferences;
import lpirm.veille.espionneapp.systems.V;

public class LoginActivity extends AppCompatActivity {

    // widgets
    private EditText edtUsername, edtPassword;
    private TextInputLayout tilUsername, tilPassword;

    // async tasks
    private LoginAsyncTask loginAsyncTask;

    // EspionPreferences
    private EspionPreferences espionPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActivityCompat.requestPermissions(this,
                V.System.retrievePermissions(this),
                0);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean grantAccess = true;

        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED)
                grantAccess = false;
        }

        if (grantAccess) {
            init();
        } else {
            Toast.makeText(this, "Il faut accepter toutes les permissions", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    // init methods
    private void init() {
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtUsername.addTextChangedListener(validateUser);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtPassword.addTextChangedListener(validateUser);
        tilUsername = (TextInputLayout) findViewById(R.id.til_username);
        tilPassword = (TextInputLayout) findViewById(R.id.til_password);

        initReceiver();
        initService();
        espionPreferences = new EspionPreferences(this);
    }

    private void initReceiver() {
        EspionReceiver rebootBroadcastReceiver = new EspionReceiver();
        IntentFilter rebootFilter = new IntentFilter(Intent.ACTION_REBOOT);
        registerReceiver(rebootBroadcastReceiver, rebootFilter);

        EspionReceiver espionBroadcastReceiver = new EspionReceiver();
        IntentFilter espionFilter = new IntentFilter(Intent.ACTION_REBOOT);
        registerReceiver(espionBroadcastReceiver, espionFilter);
    }

    private void initService() {
        Intent serviceIntent = new Intent(this, EspionService.class);
        startService(serviceIntent);
    }

    // onclick methods
    public void login(View v) {
        if (isValidUser()) {
            User user = new User();
            user.username = edtUsername.getText().toString();

            try {
                user.password = V.System.encryptPassword(edtPassword.getText().toString());

                loginAsyncTask = new LoginAsyncTask(this);
                loginAsyncTask.execute(user);
            } catch (NoSuchAlgorithmException e) {
                Toast.makeText(this, "Error au moment de l'encryptage", Toast.LENGTH_SHORT).show();
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(this, "Error au moment de la codification du mot passe", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void signup(View v) {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    // validate methods
    private boolean isValidUser() {
        if (!isValidUsername())
            return false;
        if (!isValidPassword())
            return false;

        return true;
    }

    private boolean isValidUsername() {
        String username = edtUsername.getText().toString();

        if (username.length() < 5) {
            tilUsername.setError("Ecris au moins cinq lettres");
            V.System.requestFocus(this, edtUsername);
            return false;
        } else {
            tilUsername.setErrorEnabled(false);
        }

        return true;
    }

    private boolean isValidPassword() {
        String password = edtUsername.getText().toString();

        if (password.isEmpty()) {
            tilPassword.setError("Ecris un mot de passe");
            V.System.requestFocus(this, edtPassword);
            return false;
        } else {
            tilUsername.setErrorEnabled(false);
        }

        return true;
    }

    private TextWatcher validateUser = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (edtUsername.hasFocus())
                isValidUsername();
            else if (edtPassword.hasFocus())
                isValidPassword();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    // LoginAsyncTask methods
    public void onPostLoginAsyncTask(User user) {
        if (user != null) {
            espionPreferences.setUser(user);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            showMessageDialog("Nom d'utilisateur inconnu", "Le nom d'utilisateur saisi ne semble appartenir à aucun compte. Veuillez le vérifier et réessayer.");
        }
    }

    private void showMessageDialog(String title, String message) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.show();
    }
}
