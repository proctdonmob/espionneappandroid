package lpirm.veille.espionneapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.asynctasks.SignupAsyncTask;
import lpirm.veille.espionneapp.dialogs.MessageDialog;
import lpirm.veille.espionneapp.models.User;
import lpirm.veille.espionneapp.systems.EspionPreferences;
import lpirm.veille.espionneapp.systems.V;

public class SignupActivity extends AppCompatActivity {

    // widgets
    private EditText edtUsername, edtPassword;
    private TextInputLayout tilUsername, tilPassword;
    private ImageView imvPicture;

    // async tasks
    private SignupAsyncTask signupAsyncTask;

    // EspionPreferences
    private EspionPreferences espionPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        init();

        espionPreferences = new EspionPreferences(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File picture = (File) imvPicture.getTag();

        Bitmap bitmap = BitmapFactory.decodeFile(picture.getAbsolutePath());
        imvPicture.setImageBitmap(bitmap);
    }

    // init methods
    private void init() {
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtUsername.addTextChangedListener(validateUser);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtPassword.addTextChangedListener(validateUser);
        tilUsername = (TextInputLayout) findViewById(R.id.til_username);
        tilPassword = (TextInputLayout) findViewById(R.id.til_password);
        imvPicture = (ImageView) findViewById(R.id.imv_picture);
    }

    // onclick methods
    public void signup(View v) {
        if (isValidUser()) {
            User user = new User();
            user.username = edtUsername.getText().toString();

            try {
                user.password = V.System.encryptPassword(edtPassword.getText().toString());

                File picture = (File) imvPicture.getTag();

                final Bitmap bitmap;
                if (picture == null)
                    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_espionne_app);
                else
                    bitmap = BitmapFactory.decodeFile(picture.getAbsolutePath());

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] imageByteArray = byteArrayOutputStream.toByteArray();
                user.image = Base64.encodeToString(imageByteArray, Base64.DEFAULT);

                signupAsyncTask = new SignupAsyncTask(this);
                signupAsyncTask.execute(user);
            } catch (NoSuchAlgorithmException e) {
                Toast.makeText(this, "Error au moment de l'encryptage", Toast.LENGTH_SHORT).show();
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(this, "Error au moment de la codification du mot passe", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void login(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void takePicture(View v) {
        if (isValidUsername()) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                File pictureFile = null;
                try {
                    pictureFile = createPictureFile();
                    imvPicture.setTag(pictureFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    showMessageDialog("L'image n'a pas été crée", "Nous aimerions réessayer. Cette foi-ci on va reussir!");
                }
                if (pictureFile != null) {
                    Uri pictureUri = FileProvider.getUriForFile(this, V.System.FILE_PROVIDER, pictureFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
                    startActivityForResult(cameraIntent, 0);
                }
            }
        }
    }

    private File createPictureFile() throws IOException {
        File espionneAppPictureDirectory = new File(V.System.PICTURES_DIRECTORY.toString() + "/" + getString(R.string.app_name));

        if (espionneAppPictureDirectory.exists() || espionneAppPictureDirectory.mkdirs()) {
            String pictureFileName = edtUsername.getText().toString();
            String suffix = ".jpg";

            File file = new File(espionneAppPictureDirectory, pictureFileName + suffix);

            if (file.exists())
                if (!file.delete()) throw new IOException();

            if (file.createNewFile())
                return file;
            else
                throw new IOException();
        } else throw new IOException();
    }

    // validate methods
    private boolean isValidUser() {
        if (!isValidUsername())
            return false;
        else if (!isValidPassword())
            return false;

        return true;
    }

    private boolean isValidUsername() {
        String username = edtUsername.getText().toString();

        if (username.length() < 5) {
            tilUsername.setError("Ecris au moins cinq lettres");
            V.System.requestFocus(this, edtUsername);
            return false;
        } else {
            tilUsername.setErrorEnabled(false);
        }

        return true;
    }

    private boolean isValidPassword() {
        String password = edtUsername.getText().toString();

        if (password.isEmpty()) {
            tilPassword.setError("Ecris un mot de passe");
            V.System.requestFocus(this, edtPassword);
            return false;
        } else {
            tilUsername.setErrorEnabled(false);
        }

        return true;
    }

    // text change watcher
    private TextWatcher validateUser = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (edtUsername.hasFocus())
                isValidUsername();
            else if (edtPassword.hasFocus())
                isValidPassword();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    // SignupAsyncTask methods
    public void onPostSignupAsyncTask(User user) {
        if (user != null) {
            espionPreferences.setUser(user);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else
            showMessageDialog("Non engegistré", "L'utilisateur n'a pas pu être engegistré. Veuillez réessayer.");
    }

    private void showMessageDialog(String title, String message) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.show();
    }

    private void showMessageDialog(String title, String message, View.OnClickListener acceptAction) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setAcceptAction(acceptAction);
        dialog.show();
    }
}
