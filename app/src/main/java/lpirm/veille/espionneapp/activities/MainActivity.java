package lpirm.veille.espionneapp.activities;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.asynctasks.LoginAsyncTask;
import lpirm.veille.espionneapp.dialogs.MessageDialog;
import lpirm.veille.espionneapp.services.EspionService;
import lpirm.veille.espionneapp.systems.V;

public class MainActivity extends AppCompatActivity {

    // widgets
    private TextView txvUsername;
    private ImageView imvPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    // init methods
    private void init() {
        txvUsername = (TextView) findViewById(R.id.txv_username);
        txvUsername.setText(V.user.username);
        imvPicture = (ImageView) findViewById(R.id.imv_picture);

        byte[] decodedString = Base64.decode(V.user.image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imvPicture.setImageBitmap(decodedByte);
    }

    // onclick methods
    public void logout(View v) {
        showConfirmDialog("Déconnexion", "La session sera fermée");
    }

    // dialog methods
    private void showConfirmDialog(String title, String message) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setAcceptAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        dialog.show();
    }
}
