package lpirm.veille.espionneapp.systems;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.models.Device;
import lpirm.veille.espionneapp.models.User;

/**
 * Created by Hp on 07/03/2018.
 */

public class EspionPreferences {
    private Context context;

    public EspionPreferences(Context context) {
        this.context = context;
    }

    // private methods
    private SharedPreferences getEspionPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private SharedPreferences.Editor getEspionEditor() {
        return getEspionPreferences().edit();
    }

    // setters
    public void setUser(User user) {
        if (user != null) {
            V.user = user;

            setUserId(user.id);
            setUsername(user.username);
        }
    }

    public void setUserId(int id) {
        SharedPreferences.Editor editor = getEspionEditor();
        editor.putInt(context.getString(R.string.user_id), id);
        editor.apply();
    }

    public void setUsername(String username) {
        SharedPreferences.Editor editor = getEspionEditor();
        editor.putString(context.getString(R.string.username), username);
        editor.apply();
    }

    public void setDevice(Device device) {
        if (device != null) {
            V.device = device;

            if (V.device.user != null)
                setUser(V.device.user);

            setDeviceId(device.id);
            setDeviceName(device.name);
        }
    }

    public void setDeviceId(int id) {
        SharedPreferences.Editor editor = getEspionEditor();
        editor.putInt(context.getString(R.string.device_id), id);
        editor.apply();
    }

    public void setDeviceName(String name) {
        SharedPreferences.Editor editor = getEspionEditor();
        editor.putString(context.getString(R.string.device_name), name);
        editor.apply();
    }

    public void setRebooted(boolean rebooted) {
        SharedPreferences.Editor editor = getEspionEditor();
        editor.putBoolean(context.getString(R.string.rebooted), rebooted);
        editor.apply();
    }

    // getters
    public User getUser() {
        User user = new User();
        user.id = getUserId();
        user.username = getUsername();

        return user;
    }

    public int getUserId() {
        return getEspionPreferences().getInt(context.getString(R.string.user_id), 0);
    }

    public String getUsername() {
        return getEspionPreferences().getString(context.getString(R.string.username), "-");
    }

    public Device getDevice() {
        Device device = new Device();
        device.id = getDeviceId();
        device.name = getDeviceName();

        return device;
    }

    public int getDeviceId() {
        return getEspionPreferences().getInt(context.getString(R.string.device_id), 0);
    }

    public String getDeviceName() {
        return getEspionPreferences().getString(context.getString(R.string.device_name), "-");
    }

    // methods
    public boolean wasRebooted() {
        return getEspionPreferences().getBoolean(context.getString(R.string.rebooted), false);
    }
}
