package lpirm.veille.espionneapp.systems;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import lpirm.veille.espionneapp.R;
import lpirm.veille.espionneapp.models.Device;
import lpirm.veille.espionneapp.models.User;

/**
 * Created by Hp on 06/03/2018.
 */

public abstract class V {
    public static abstract class System {
        // TODO constants

        public static final String PACKAGE_NAME = "lpirm.veille.espionneapp";
        public static final String ESPION_RECEIVER_ACTION = PACKAGE_NAME + ".EspionReceiver";
        public static final String FILE_PROVIDER = PACKAGE_NAME + ".FileProvider";

        public static void requestFocus(Activity activity, View v) {
            if (v.requestFocus()) {
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        }

        // TODO encryptPassword
        public static String encryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));

            return new BigInteger(1, crypt.digest()).toString(16);
        }

        public static String getLocalBluetoothName() {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            String name = mBluetoothAdapter.getName();
            if (name == null) {
                name = mBluetoothAdapter.getAddress();
            }
            return name;
        }

        public static String[] retrievePermissions(Context context) {
            try {
                return context
                        .getPackageManager()
                        .getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS)
                        .requestedPermissions;
            } catch (PackageManager.NameNotFoundException e) {
                throw new RuntimeException("This should have never happened.", e);
            }
        }
    }

    public static User user = new User();
    public static Device device = new Device();
}
