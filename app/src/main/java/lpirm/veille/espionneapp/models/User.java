package lpirm.veille.espionneapp.models;

import java.util.List;

public class User {
    public int id;
    public String username;
    public String password;
    public String image;
    public List<Device> devices;
}
