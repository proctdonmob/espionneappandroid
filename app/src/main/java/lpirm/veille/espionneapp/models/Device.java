package lpirm.veille.espionneapp.models;

import java.util.List;

/**
 * Created by Hp on 06/03/2018.
 */

public class Device {
    public int id;
    public String name;
    public String version;
    public double lat;
    public double lng;
    public List<File> files;
    public User user;
}
