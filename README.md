# EspionneAppAndroid

This project contains the EspionneApp and the EspionneService.
 
## EspionneApp

Allow user to signup and login.

## EspionneService

Erase the user photo and read the device information.

## Code architecture

Each component must be created in it respective package directory.

Example:

Component Type | Component Name | Package
--- | --- | ---
Activity | MainActivity.java | fr.lr.iut.espionneappandroid.activities
Adapter | UsersAdapter.java | fr.lr.iut.espionneappandroid.adapters
Model | Model.java | fr.lr.iut.espionneappandroid.models
AsyncTask | LoginAsyncTask.java | fr.lr.iut.espionneappandroid.asynctasks

## Code convenction

### Class name

The class name must be written in UpperCamleCase.

Example:
```java
public class MainActivity extends Activity {
  // ...
}
```

#### Components type

##### Java files nomenclature

Each class component file name must have the suffix of component.
 
Example:
```java
public class MainActivity extends Activity {
    // ...
}

public class LoginAsyncTask extends AsyncTask<String, Void, Object> {
    // ...
}
```

###### Commented code

Each java file must be organized using comments.

Example:
```java
public class MainActivity extends Activity {
    // widgets
    private EditText edtUsername;
    private EditText edtPassword;
    private Button btnLogin;
    
    // overrides methods
    @override
    protected void onCreate(Bundle savedInstanceState) {
        // ...
    }
    
    // methods
}

public class User {
    // attributes
    private String username;
    private String password;
    
    // constructors
    public User() {
        username = password = "";
    }
    
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    // getters
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
    
    // setters
    public void setUsername(String username) {
        this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    // methods
}
```

##### Resource XML files nomenclature

Each resource file name must be written in lowercase and have the prefix of component. Use `_` to separate the words.

Example:

Component Type | XML File | Java File
--- | --- | ---
Activity | activity_main.xml | MainActivity.java
List row  | list_row_user.xml | UsersAdapter.java

### Variables names

The variables names must be written in LowerCamelCase. Don't use acronyms, incomplete names or one letter names. If the variable is a component (Adapter, Button, ListView) must have three letters prefix to identify the component.   

Example:
```java
public class MainActivity extends Activity {
    // widgets
    private EditText edtUsername;
    private EditText edtPassword;
    private Button btnLogin;
    
    // overrides methods
    @override
    protected void onCreate(Bundle savedInstanceState) {
        // ...
    }
    
    // methods
}
```

#### Table of prefix

##### Widgets
Component Type | Prefix
--- | ---
Button | btn
ToggleButton | tbt
CheckBox | ckb
RadioButton | rbt
CheckedTextView | ctv
Spinner | spn
ProgressBar | pgb
SeekBar | skb
QuickContactBadge | qcb
RatingBar | rtb
Switch | swt
Space | spc

##### Text
Component Type | Prefix
--- | ---
TextView | txv
EditText | edt
AutoCompleteTextView | act
MultiAutoCompleteTextView | mac

##### Layouts
Component Type | Prefix
--- | ---
ConstraintLayout | ctl
GridLayout | gdl
FrameLayout | fml
LinearLayout | lnl
RelativeLayout | rvl
TableLayout | tbl
TableRow | tbr
fragment | frg

##### Containers
Component Type | Prefix
--- | ---
RadioGroup | rdg
ListView | lsv
GridView | gdv
ExpandableListView | elv
ScrollView | scv
HorizontalScrollView | hsv
TabHost | tbh
WebView | wbv
SearchView | srv
ViewPager | vpg

##### Images
Component Type | Prefix
--- | ---
ImageButton | imb
ImageView | imv
VideoView | vdv

##### Date
Component Type | Prefix
--- | ---
TimePicker | tpk
DatePicker | dpk
CalendarView| cdv
Chronometer | crm
TextClock | txc

##### Transitions
Component Type | Prefix
--- | ---
ImageSwitcher | ims
AdapterViewFlipper | avf
StackView | stv
TextSwitcher | txs
ViewAnimator | vam
ViewFlipper | vfp
ViewSwitcher | vsc

##### Advanced
Component Type | Prefix
--- | ---
include | inc
requestFocus | rqf
view | viw
ViewStub | vst
TextureView | ttv
SurfaceView | sfv
NumberPicker | npk

##### Google
Component Type | Prefix
--- | ---
AdView | adv
MapView | mpv

##### Design
Component Type | Prefix
--- | ---
CoordinatorLayout | cdl
AppBarLayout | apl
TabLayout | tab
TabItem | tbi
NestedScrollView | nsv
FloatingActionButton | fab
TextInputLayout | til

##### AppCompat
Component Type | Prefix
--- | ---
CardView | cdv
RecyclerView | rcv
Toolbar | tlb
